import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

/**
 * @author DJOUDER Abdenour
 */
public class Partition {

    /**
     * @param source:     list of integers
     * @param sublistSize
     * @return @List<@List<@Integer>> : list of sublists with a source list size concatenated with a list with
     * the size of the rest of source.size()%sublistSize
     */
    public List<List<Integer>> partition(List<Integer> source, int sublistSize) {
        List<List<Integer>> result = new ArrayList<>();

        if (validateArguments().apply(source, sublistSize)) {
            int sourceLength = source.size();

            int modulo_reminder = sourceLength % sublistSize;
            int currentSublistStartIndex = 0;

            // populate the sublist with the given sulistSize
            while (currentSublistStartIndex < sourceLength - modulo_reminder) {
                int currentSublistEndIndex = currentSublistStartIndex + sublistSize;
                result.add(source.subList(currentSublistStartIndex, currentSublistEndIndex));
                currentSublistStartIndex = currentSublistStartIndex + sublistSize;
            }

            // populate the rest of the list
            if (modulo_reminder > 0) {
                final int secondPartLength = sourceLength % sublistSize;
                result.add(source.subList(sourceLength - secondPartLength, sourceLength));
            }
        }
        return result;

    }

    /**
     * @return BiFunction which validates the list of the integer source and the length of the sublist
     */
    private BiFunction<List<Integer>, Integer, Boolean> validateArguments() throws PartitionException {
        BiFunction<List<Integer>, Integer, Boolean> control = (List<Integer> integers, Integer sublistLength) -> {

            // vérifier si la liste est null
            if (!Optional.ofNullable(integers).isPresent()) {
                throw new PartitionException("Le tableau source est null");
            }

            // Vérifiaction de la taille  des sous listes
            if (!integers.isEmpty() && (sublistLength == 0 || sublistLength > integers.size())) {
                throw new PartitionException("La taille des sous listes doit être comprise entre 1 et la la longueure du tableau source");
            }

            return !integers.isEmpty();
        };
        return control;
    }

}