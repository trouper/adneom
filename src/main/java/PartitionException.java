public class PartitionException extends RuntimeException {
    private String message;

    PartitionException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
