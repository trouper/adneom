import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class PartitionTest {

    @Test
    public void testPartition_with_modulo_reminder_different_to_zero_expected_sublists_same_length_except_one() {
        Partition partition = new Partition();
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> source = Arrays.asList(numbers);
        List<List<Integer>> partitions = partition.partition(source, 3);

        Assertions.assertThat(partitions.size()).isEqualTo(4);
        Assertions.assertThat(partitions.get(0)).contains(1, 2, 3);
        Assertions.assertThat(partitions.get(1)).contains(4, 5, 6);
        Assertions.assertThat(partitions.get(2)).contains(7, 8, 9);
        Assertions.assertThat(partitions.get(3)).contains(10);
    }

    @Test
    public void testPartition_with_modulo_reminder_zero_expected_sublist_with_same_length() {
        Partition partition = new Partition();
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> source = Arrays.asList(numbers);
        List<List<Integer>> partitions = partition.partition(source, 5);

        Assertions.assertThat(partitions.size()).isEqualTo(2);
        Assertions.assertThat(partitions.get(0)).contains(1, 2, 3, 4, 5);
        Assertions.assertThat(partitions.get(1)).contains(6, 7, 8, 9, 10);
    }

    @Test
    public void testPartition_with_empty_list_expected_empty_list() {
        Partition partition = new Partition();
        List<List<Integer>> partitions = partition.partition(new ArrayList<>(), 5);
        Assertions.assertThat(partitions.size()).isEqualTo(0);
    }

    @Test(expected = PartitionException.class)
    public void testPartition_with_null_source_expected_argument_exception() {
        Partition partition = new Partition();
        List<List<Integer>> partitions = partition.partition(null, 5);
        Assertions.assertThat(partitions.size()).isEqualTo(0);
    }

    @Test(expected = PartitionException.class)
    public void testPartition_with_size_more_then_the_size_of_the_source_expected_length_argument_exception() {
        Partition partition = new Partition();
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> source = Arrays.asList(numbers);
        partition.partition(source, 15);
    }

    @Test(expected = PartitionException.class)
    public void testPartition_with_size_zero_expected_length_argument_exception() {
        Partition partition = new Partition();
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> source = Arrays.asList(numbers);
        partition.partition(source, 0);
    }

    @Test
    public void testPartition_with_size_equal_then_the_size_of_the_source_expected_one_sublist() {
        Partition partition = new Partition();
        Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        List<Integer> source = Arrays.asList(numbers);
        List<List<Integer>> partitions = partition.partition(source, 10);
        Assertions.assertThat(partitions.size()).isEqualTo(1);
        Assertions.assertThat(partitions.get(0)).contains(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    }
}
